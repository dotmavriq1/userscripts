# Userscripts 
#### for ViolentMonkey, Greasemonkey, Tampermonkey et.c.

---



### A collection of userscripts I have made for websites I visit every now and then.
#### These userscripts can be enabled in your browser using ViolentMonkey, Tampermonkey or Greasemonkey and should work in all the browsers that support these extensions.

<img src="gruvec.jpg" alt="picture of Emocore.se with my Gruvbox and SinBlock userscript activated." />